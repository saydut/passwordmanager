import os
import requests
import sys
import subprocess
import tempfile
import zipfile
import sqlite3
from PyQt5.QtWidgets import (
    QMenu, QAction, QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout,
    QLabel, QDialog, QLineEdit, QPushButton, QMessageBox, QTableWidget, QCheckBox, QInputDialog, QTableWidgetItem)
from PyQt5.QtCore import Qt
import qdarkstyle
import ctypes
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

class EncryptionApp:
    def __init__(self):
        pass

    def key_derivation(self):
        # Sabit bir tuz (salt) kullanabiliriz, çünkü parola girişi yok
        salt = b'\x00' * 16

        # Sabit bir parola kullanabiliriz, çünkü kullanıcıdan giriş beklemiyoruz
        password = b'sabit_parola'

        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            iterations=100000,
            salt=salt,
            length=32,
            backend=default_backend()
        )
        return kdf.derive(password)

    def encrypt_file(self, file_path, key):
        # Dosyanın içeriğini oku
        with open(file_path, 'rb') as f:
            plaintext = f.read()

        # Şifreleme işlemi için AES-GCM şifreleme algoritmasını kullan
        cipher = Cipher(algorithms.AES(key), modes.GCM(b'\x00' * 16), backend=default_backend())
        encryptor = cipher.encryptor()

        # Dosyanın içeriğini şifrele
        ciphertext = encryptor.update(plaintext) + encryptor.finalize()

        # Şifrelenmiş veriyi yeni bir dosyaya kaydet
        with open(file_path + '.enc', 'wb') as f:
            f.write(encryptor.tag + b'\x00' * 16 + ciphertext)

        # Şifresiz verileri sil (orijinal dosyayı sil)
        os.remove(file_path)

        print(f"{file_path} dosyası başarıyla şifrelendi.")

    def decrypt_file(self, encrypted_file_path, key):
        with open(encrypted_file_path, 'rb') as f:
            data = f.read()
            tag = data[:16]
            salt = data[16:32]
            ciphertext = data[32:]

        cipher = Cipher(algorithms.AES(key), modes.GCM(salt, tag), backend=default_backend())
        decryptor = cipher.decryptor()
        plaintext = decryptor.update(ciphertext) + decryptor.finalize()

        decrypted_file_path = encrypted_file_path[:-4]  # Remove the '.enc' extension
        with open(decrypted_file_path, 'wb') as f:
            f.write(plaintext)

        os.remove(encrypted_file_path)  # Şifreli verileri sil

        print(f"{decrypted_file_path} dosyası başarıyla çözüldü.")

    def check_and_decrypt_main_password(self):
        encrypted_file_path = "mainpassword.db.enc"

        if os.path.exists(encrypted_file_path):
            key = self.key_derivation()
            self.decrypt_file(encrypted_file_path, key)
        else:
            print(f"{encrypted_file_path} dosyası bulunamadı.")

    def check_and_encrypt_main_password(self):
        file_path = "mainpassword.db"

        if os.path.exists(file_path):
            key = self.key_derivation()
            self.encrypt_file(file_path, key)
        else:
            print(f"{file_path} dosyası bulunamadı.")

class PasswordManager:
    def __init__(self):
        self.app = QApplication([])
        self.main_window = QMainWindow()
        self.new_window = None
        self.db_path = os.path.join(os.getcwd(), 'mainpassword.db')
        self.conn = sqlite3.connect(self.db_path)
        self.cursor = self.conn.cursor()
        
        self.version_label = QLabel('Sürüm: 1.1.5')
        self.version_label.setAlignment(Qt.AlignRight)
        
        self.create_master_password_table()
        self.apply_windows_theme()
        
    def create_menu(self):
        main_menu = self.main_window.menuBar()
        view_menu = main_menu.addMenu('Temalar')

        dark_theme_action = QAction('Karanlık Tema', self.main_window)
        dark_theme_action.setStatusTip('Karanlık Tema Uygula')
        dark_theme_action.triggered.connect(self.apply_dark_theme)
        view_menu.addAction(dark_theme_action)

        light_theme_action = QAction('Açık Tema', self.main_window)
        light_theme_action.setStatusTip('Açık Tema Uygula')
        light_theme_action.triggered.connect(self.apply_light_theme)
        view_menu.addAction(light_theme_action)

    def apply_dark_theme(self):
        self.main_window.setStyleSheet(qdarkstyle.load_stylesheet(qt_api='pyqt5'))

    def apply_light_theme(self):
        self.main_window.setStyleSheet('')

    def apply_windows_theme(self):
        is_dark_mode = self.is_dark_mode()
        if is_dark_mode:
            self.apply_dark_theme()
        else:
            self.apply_light_theme()

    def is_dark_mode(self):
        try:
            registry_path = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize'
            value_name = 'AppsUseLightTheme'
            key = ctypes.c_void_p()

            result = ctypes.windll.advapi32.RegOpenKeyExW(
                ctypes.c_uint32(0x80000001),  # HKEY_CURRENT_USER
                ctypes.c_wchar_p(registry_path),
                0,
                0x20019,  # KEY_READ | KEY_WOW64_64KEY
                ctypes.byref(key)
            )

            if result != 0:
                raise Exception("Error opening registry key")

            value = ctypes.c_uint32()
            value_size = ctypes.c_uint32(ctypes.sizeof(value))
            result = ctypes.windll.advapi32.RegQueryValueExW(
                key,
                ctypes.c_wchar_p(value_name),
                None,
                None,
                ctypes.byref(value),
                ctypes.byref(value_size)
            )

            ctypes.windll.advapi32.RegCloseKey(key)

            if result == 0:
                return value.value == 0
            else:
                raise Exception("Error querying registry value")

        except Exception as e:
            print(f"Error detecting theme: {e}")
            return False

    def is_light_mode(self):
        try:
            registry_path = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize'
            value_name = 'AppsUseLightTheme'
            key = ctypes.c_void_p()

            result = ctypes.windll.advapi32.RegOpenKeyExW(
                ctypes.c_uint32(0x80000001),  # HKEY_CURRENT_USER
                ctypes.c_wchar_p(registry_path),
                0,
                0x20019,  # KEY_READ | KEY_WOW64_64KEY
                ctypes.byref(key)
            )

            if result != 0:
                raise Exception("Error opening registry key")

            value = ctypes.c_uint32()
            value_size = ctypes.c_uint32(ctypes.sizeof(value))
            result = ctypes.windll.advapi32.RegQueryValueExW(
                key,
                ctypes.c_wchar_p(value_name),
                None,
                None,
                ctypes.byref(value),
                ctypes.byref(value_size)
            )

            ctypes.windll.advapi32.RegCloseKey(key)

            if result == 0:
                return value.value == 1
            else:
                raise Exception("Error querying registry value")

        except Exception as e:
            print(f"Error detecting theme: {e}")
            return False
    def create_master_password_table(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS master_password (password TEXT)''')

    def check_master_password(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS passwords (hesap TEXT, kullanici TEXT, sifre TEXT)''')
        self.cursor.execute('''SELECT * FROM master_password''')
        row = self.cursor.fetchone()
        if row is None:
            self.create_master_password()
        else:
            self.verify_master_password()

    def create_master_password(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS master_password (password TEXT)''')
        password, ok = QInputDialog.getText(None, "Ana Şifre", "Lütfen bir ana şifre belirleyin:")
        if ok:
            self.cursor.execute('''INSERT INTO master_password (password) VALUES (?)''', (password,))
            self.conn.commit()
            QMessageBox.information(None, "Başarılı", "Ana şifre oluşturuldu!")
            self.verify_master_password()
        else:
            QMessageBox.warning(None, "Uyarı", "Lütfen bir şifre girin.")
            self.conn.close()

    def verify_master_password(self):
        password, ok = QInputDialog.getText(None, "Ana Şifre", "Lütfen ana şifreyi girin:")
        if ok:
            self.cursor.execute('''SELECT * FROM master_password WHERE password = ?''', (password,))
            row = self.cursor.fetchone()
            if row:
                self.conn.close()
                self.open_main_application()
            else:
                QMessageBox.critical(None, "Hata", "Yanlış şifre!")
                self.conn.close()
        else:
            QMessageBox.warning(None, "Uyarı", "Lütfen bir şifre girin.")
            self.conn.close()

    def open_main_application(self):
        self.main_window.setWindowTitle("Şifre Yönetici")
        self.create_widgets()
        self.main_window.show()
        self.app.exec_()

    def create_widgets(self):
        central_widget = QWidget()
        self.main_window.setCentralWidget(central_widget)

        layout = QVBoxLayout()
        central_widget.setLayout(layout)

        frame = QWidget()
        frame_layout = QVBoxLayout()
        frame.setLayout(frame_layout)
        
        layout.addWidget(self.version_label)

        label1 = QLabel("Hesap İsmi:")
        self.hesap_entry = QLineEdit()
        frame_layout.addWidget(label1)
        frame_layout.addWidget(self.hesap_entry)

        label2 = QLabel("Kullanıcı Adı/E-Posta:")
        self.kullanici_entry = QLineEdit()
        frame_layout.addWidget(label2)
        frame_layout.addWidget(self.kullanici_entry)

        label3 = QLabel("Şifre:")
        self.sifre_entry = QLineEdit()
        self.sifre_entry.setEchoMode(QLineEdit.Password)
        frame_layout.addWidget(label3)
        frame_layout.addWidget(self.sifre_entry)

        button_frame = QWidget()
        button_frame_layout = QHBoxLayout()
        button_frame.setLayout(button_frame_layout)

        self.kaydet_button = QPushButton("Kaydet")
        self.bilgi_al_button = QPushButton("Bilgi Al")
        self.kayitli_bilgiler_button = QPushButton("Kayıtlı Bilgiler")
        self.guncelleme_kontrol_button = QPushButton("Güncelleme Kontrolü")

        self.kaydet_button.clicked.connect(self.kaydet)
        self.bilgi_al_button.clicked.connect(self.bilgi_al)
        self.kayitli_bilgiler_button.clicked.connect(self.kayitli_bilgiler)
        self.guncelleme_kontrol_button.clicked.connect(self.prog_ver_check)

        button_frame_layout.addWidget(self.kaydet_button)
        button_frame_layout.addWidget(self.bilgi_al_button)
        button_frame_layout.addWidget(self.kayitli_bilgiler_button)
        button_frame_layout.addWidget(self.guncelleme_kontrol_button)

        layout.addWidget(frame)
        layout.addWidget(button_frame)
        
    def find_uninstaller(self):
        # Uninstaller'ı bul
        uninstallers = ["unins000.exe", "uninstall.exe"]  # İhtiyaca göre diğer uninstaller isimlerini de ekleyebilirsiniz.
        for uninstaller in uninstallers:
            uninstaller_path = os.path.join(os.path.dirname(sys.executable), uninstaller)
            if os.path.exists(uninstaller_path):
                return uninstaller_path
        return None

    def create_uninstall_script(self, current_exe_path):
        # Uninstall scriptini oluştur
        uninstaller_path = self.find_uninstaller()
        if not uninstaller_path:
            print("Uninstaller bulunamadı.")
            return None

        uninstall_bat_content = f'@echo off\n'
        uninstall_bat_content += f'start "" "{uninstaller_path}" /S\n'  # Uninstaller'ı sessiz modda başlat
        uninstall_bat_content += f'ping 127.0.0.1 -n 2 > nul\n'  # Kısa bir bekleme
        uninstall_bat_content += f'ping 127.0.0.1 -n 10 > nul\n'
        uninstall_bat_content += f'start "" "{tempfile.gettempdir()}\\update_script.bat"\n'  # Update script'ini başlat
        uninstall_bat_path = os.path.join(tempfile.gettempdir(), "uninstall_script.bat")
        with open(uninstall_bat_path, 'w') as uninstall_bat_file:
            uninstall_bat_file.write(uninstall_bat_content)
        return uninstall_bat_path

    def run_uninstall_script(self, uninstall_bat_path):
        try:
            subprocess.Popen(uninstall_bat_path, shell=True)
            return True
        except Exception as e:
            print(f"Hata: {e}")
            return False

    def create_update_script(self, temp_exe_path, current_exe_path):
        # Bat dosyasını oluştur
        bat_content = f'@echo off\n'
        bat_content += f'start "" "{temp_exe_path}"\n'
        bat_content += f'exit\n'  # Bat dosyasından çık
        bat_path = os.path.join(tempfile.gettempdir(), "update_script.bat")
        with open(bat_path, 'w') as bat_file:
            bat_file.write(bat_content)
        return bat_path

    def run_update_script(self, bat_path):
        try:
            subprocess.Popen(bat_path, shell=True)
            return True
        except Exception as e:
            print(f"Hata: {e}")
            return False

    def extract_release_notes(self, version_txt_content):
        # version.txt içinden yenilik notlarını çıkar
        start_index = version_txt_content.find('*')
        release_notes = []

        while start_index != -1:
            end_index = version_txt_content.find('\n', start_index)
            if end_index != -1:
                note = version_txt_content[start_index:end_index].strip('* \t\n')
                release_notes.append(note)
            start_index = version_txt_content.find('*', end_index)

        if release_notes:
            return '\n'.join(release_notes)
        else:
            return "Yenilik notları bulunamadı."

    def extract_zip(self, zip_path, extract_path):
        with zipfile.ZipFile(zip_path, 'r') as zip_ref:
            zip_ref.extractall(extract_path)
        return extract_path

    def get_single_exe_in_directory(self, directory):
        # Verilen dizindeki tek exe dosyasını bul
        exe_files = [f for f in os.listdir(directory) if f.endswith('.exe')]
        if len(exe_files) == 1:
            return os.path.join(directory, exe_files[0])
        else:
            raise ValueError("Dizinde tek bir exe dosyası olmalı.")

    def download_update(self, url, dest_path):
        try:
            response = requests.get(url, stream=True)
            with open(dest_path, 'wb') as file:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        file.write(chunk)
        except Exception as e:
            print(f"Hata: {e}")

    def prog_ver_check(self):
        gitlab_url = "https://gitlab.com/saydut/passwordmanager/-/raw/main/version.txt"

        local_version = self.version_label.text().split(":")[-1].strip()

        try:
            response = requests.get(gitlab_url)
            remote_version_info = response.text.strip().split('\n')
            remote_version = remote_version_info[0]
            release_notes_start = response.text.find('*')
            release_notes = response.text[release_notes_start:].split('link=')[0].strip()
            update_link_start = response.text.find('link=')
            update_link = response.text[update_link_start + 5:].strip()

            if remote_version > local_version:
                # Yeni sürüm mevcut! Güncelleme yapılabilir.
                notes_response = requests.get(gitlab_url)
                release_notes = self.extract_release_notes(notes_response.text)

                reply = QMessageBox.question(self.main_window, f'Yeni Sürüm ({remote_version})',
                                             f"Yeni bir sürüm mevcut!\nGüncelleme yapmak ister misiniz?\n\n{release_notes}",
                                             QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

                if reply == QMessageBox.Yes:
                    # Kullanıcı evet dediği durum
                    print("Güncelleme işlemleri burada başlayacak.")
                    temp_zip_path = os.path.join(tempfile.gettempdir(), "temp_update.zip")
                    temp_exe_path = os.path.join(tempfile.gettempdir(), "temp_update.exe")

                    # Zip dosyasını indir ve çıkar
                    self.download_update(update_link, temp_zip_path)
                    extracted_path = self.extract_zip(temp_zip_path, tempfile.gettempdir())  # Zip dosyasını çıkar
                    exe_path = self.get_single_exe_in_directory(extracted_path)  # Çıkarılan dizindeki tek exe dosyasını al

                    # Bat dosyalarını oluştur ve çalıştır
                    update_bat_path = self.create_update_script(exe_path, sys.executable)
                    uninstall_bat_path = self.create_uninstall_script(sys.executable)

                    # Uninstall script'ini çalıştır
                    self.run_uninstall_script(uninstall_bat_path)

                    # Güncelleme script'ini çalıştıktan sonra programı kapat
                    sys.exit()

            else:
                # Program güncel.
                QMessageBox.information(self.main_window, 'Program Sürüm Kontrolü', 'Program zaten güncel', QMessageBox.Ok)
        except requests.RequestException as e:
            print(f"Hata: {e}")



    def kaydet(self):
        hesap = self.hesap_entry.text()
        kullanici = self.kullanici_entry.text()
        sifre = self.sifre_entry.text()

        if hesap and kullanici and sifre:
            conn = sqlite3.connect(self.db_path)
            cursor = conn.cursor()
            cursor.execute('''CREATE TABLE IF NOT EXISTS passwords (hesap TEXT, kullanici TEXT, sifre TEXT)''')
            cursor.execute('''INSERT INTO passwords VALUES (?, ?, ?)''', (hesap, kullanici, sifre))
            conn.commit()
            conn.close()
            QMessageBox.information(None, "Başarılı", "Bilgiler kaydedildi.")
        else:
            QMessageBox.warning(None, "Hata", "Lütfen tüm alanları doldurun.")

    def bilgi_al(self):
        hesap = self.hesap_entry.text()

        if hesap:
            conn = sqlite3.connect(self.db_path)
            cursor = conn.cursor()
            cursor.execute('''SELECT * FROM passwords WHERE hesap = ?''', (hesap,))
            rows = cursor.fetchall()

            if rows:
                info = ""
                for row in rows:
                    info += f"Hesap: {row[0]}, Kullanıcı: {row[1]}, Şifre: {row[2]}\n"
                QMessageBox.information(None, "Bilgiler", info)
            else:
                QMessageBox.warning(None, "Uyarı", "Hesap bulunamadı.")
            conn.close()
        else:
            QMessageBox.warning(None, "Hata", "Lütfen hesap ismi girin.")

    def kayitli_bilgiler(self):
        if self.new_window and self.new_window.isVisible():
            self.new_window.close()
        
        self.new_window = QWidget()
        self.new_window.setWindowTitle("Kayıtlı Bilgiler")
        self.new_window.setGeometry(100, 100, 440, 250)  # Boyutları ayarladım

        layout = QVBoxLayout()
        self.new_window.setLayout(layout)

        conn = sqlite3.connect(self.db_path)
        cursor = conn.cursor()
        cursor.execute('''SELECT * FROM passwords''')
        rows = cursor.fetchall()

        table = QTableWidget()
        table.setColumnCount(4)
        table.setHorizontalHeaderLabels(["Hesap", "Kullanıcı Adı/E-Posta", "Şifre", "Seç"])

        self.checkboxes = []

        for i, row in enumerate(rows):
            table.insertRow(i)

            for j, item in enumerate(row):
                if j == 0:
                    if len(item) > 10:
                        item = item[:10] + '...'
                elif j == 1:
                    if len(item) > 25:
                        item += '...' * (25 - len(item))
                elif j == 2:
                    if len(item) > 10:
                        item = item[:10] + '...'
                table.setItem(i, j, QTableWidgetItem(str(item)))
                
            

            checkbox = QCheckBox()
            checkbox.setChecked(False)  # başlangıçta unchecked.
            self.checkboxes.append(checkbox)

            cell_widget = QWidget()
            cell_layout = QHBoxLayout(cell_widget)
            cell_layout.addWidget(checkbox)
            cell_layout.setAlignment(Qt.AlignCenter)
            cell_layout.setContentsMargins(0, 0, 0, 0)
            table.setCellWidget(i, 3, cell_widget)  # tabloyu checkboxa ekleme
            table.setColumnWidth(3, 50)  # sütun kalınlığı
            table.setColumnWidth(0, 70)
            table.setColumnWidth(1, 170)
            table.setColumnWidth(2, 100)
            
        def show_context_menu(pos):
            menu = QMenu()
            copy_action = menu.addAction("Kopyala")
            action = menu.exec_(table.viewport().mapToGlobal(pos))
            
            if action == copy_action:
                selected_item = table.itemAt(pos)
                if selected_item:
                    clipboard = QApplication.clipboard()
                    clipboard.setText(selected_item.text())


        def delete_selected():
            selected_indices = [i for i, checkbox in enumerate(self.checkboxes) if checkbox.isChecked()]
            if selected_indices:
                conn = sqlite3.connect(self.db_path)
                cursor = conn.cursor()
                for index in selected_indices:
                    cursor.execute('''DELETE FROM passwords WHERE rowid=?''', (index + 1,))
                conn.commit()
                conn.close()
                QMessageBox.information(self.new_window, "Başarılı", "Seçilen kayıtlar silindi.")
                self.kayitli_bilgiler()  # Yeniden aç

        table.setContextMenuPolicy(Qt.CustomContextMenu)
        table.customContextMenuRequested.connect(show_context_menu)
        
        delete_button = QPushButton("Seçilenleri Sil")
        delete_button.clicked.connect(delete_selected)

        layout.addWidget(table)
        layout.addWidget(delete_button)

        self.new_window.setLayout(layout)
        self.new_window.show()
        
class CombinedApp:
    def __init__(self):
        self.encryption_app = EncryptionApp()
        self.password_manager = PasswordManager()

    def run(self):
        self.encryption_app.check_and_decrypt_main_password()
        self.password_manager.check_master_password()
        self.encryption_app.check_and_encrypt_main_password()


if __name__ == "__main__":
    combined_app = CombinedApp()
    combined_app.run()